/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mineboum2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;

/**
 *
 * @author christine
 */
public class MineBoumApp  implements ActionListener, MouseListener {

    
   public  MineBoumApp() {
        
        
        
        affichage = new MineBoumView(this);
        affichage.setEcouteur(this);
        affichage.setVisible(true);
        
        // initialisation
        
        int [][] tab = new int[4][4];
        
        plateau = new Plateau(tab);
        
       plateau.addObserver(affichage);
        affichage.definirLeTableau(tab);
        plateau.nouveauPlateauAleatoire(6, 6, 6);
        
        
        
    }

   
   
    private MineBoumView affichage;
    private Plateau plateau;
    
  
                
        
    
    
    
    
    
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        System.out.println(e.getActionCommand());
         if (e.getActionCommand().split(":")[0].equals("NEWGAME"))
           {
            int x = Integer.valueOf(e.getActionCommand().split(":")[1]);
            int y = Integer.valueOf(e.getActionCommand().split(":")[2]);
            
           plateau.nouveauPlateauAleatoire(x, x, y);
            
           }
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
      
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.print("\nClick");
          Object obj = e.getSource();
       if( obj instanceof JButton )
       {
           
        System.out.print(" boutton");
           JButton  butt = (JButton) obj;
           if (butt.getActionCommand().split(":")[0].equals("BUTTON"))
           {
            int y = Integer.valueOf(butt.getActionCommand().split(":")[1]);
            int x = Integer.valueOf(butt.getActionCommand().split(":")[2]);
            
            System.out.print("("+x+","+y+")");
            if(e.getButton()== MouseEvent.BUTTON1)
                
                
            if (plateau.isABomb(x, y)) plateau.setPartiePerdue(true);
            else if (plateau.isEmpty(x, y))plateau.revellerUneZone(x, y);
            else plateau.revellerUneZone(x, y);   
               
            if(e.getButton()== MouseEvent.BUTTON3)
            {
             
            
            }
           }
           
       
       }
       
       
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    
}
