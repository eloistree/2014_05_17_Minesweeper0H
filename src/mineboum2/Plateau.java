/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mineboum2;

import java.awt.Point;
import java.util.LinkedList;
import java.util.Observable;

public class Plateau extends Observable {
    
    private int[][] tableau;
    private boolean[][] tableauZoneRevellee;
    private int xTableau;
    private int yTableau;
    private boolean partiePerdue;
    private boolean partieGagner;
    
    public Plateau(int[][] _tableau) {
        tableau = _tableau;
        yTableau = tableau.length;
        xTableau = tableau[0].length;
        tableauZoneRevellee = new  boolean[yTableau][xTableau];
        
        
    }
    
    public void incrementerUneCase(int x, int y) {
        
        System.out.print(" increment");
        if (x < xTableau && y < yTableau) {
            tableau[y][x]++;
            
            setChanged();
            notifyObservers(tableau);
        }
        
    }
    
    
    
    public void decrementerUneCase(int x, int y) {
        System.out.print(" decrement");
        if (x < xTableau && y < yTableau && tableau[y][x] > 0) {
            tableau[y][x]--;
            
            
            setChanged();
            notifyObservers(tableau);
        }
    }
    
    
    public int[][] getTableau() {
        return tableau;
    }

    public boolean[][] getTableauZoneRevellee() {
        return tableauZoneRevellee;
    }
    
    
    public void setTableau(int[][] _tableau) {
        tableau = _tableau;
        yTableau = tableau.length;
        xTableau = tableau[0].length;
        tableauZoneRevellee = new  boolean[yTableau][xTableau];
        setChanged();
        notifyObservers(tableau);
    }
    
    public void nouveauPlateauAleatoire(int _x, int _y, int _nombreDeMine) {
        if (_x *_y > _nombreDeMine) {
            partiePerdue =false;
            LinkedList<Point> positionMines = new LinkedList<Point>();
            
            int[][] tab = new int[_y][_x];
            while (_nombreDeMine > 0) {
                int x = BoiteAOutil.nombreAléatoireEntreA_B(0, _x);
                int y = BoiteAOutil.nombreAléatoireEntreA_B(0, _y);
                
                if (tab[y][x] == 0) {
                    tab[y][x] = 10;
                    _nombreDeMine--;
                    positionMines.add(new Point(x, y));
                    
                }
                
            }
            entourerLesMinesDeMagnetismes(tab, positionMines);
            setTableau(tab);
            
        }
        
        
    }

    public boolean isPartiePerdue() {
        return partiePerdue;
        
    }

    public void setPartiePerdue(boolean partiePerdue) {
        this.partiePerdue = partiePerdue;
        if (partiePerdue ==true) 
        {
        setChanged();
        notifyObservers();
        }
    }
    
    public void revellerUneZone(int _x, int _y)
    {
     tableauZoneRevellee[_y][_x]=true;
     setChanged();
     notifyObservers(tableau);
    
    };
    public void camouflerUneZone(int _x, int _y)
    {
     tableauZoneRevellee[_y][_x]=false;
     setChanged();
     notifyObservers(tableau);
    
    };
    public boolean isABomb(int _x, int _y) {
        if (tableau[_y][_x] >= 9) {
            return true;
        }
        return false;
    }

    public boolean isEmpty(int _x, int _y) {
        if (tableau[_y][_x] == 0) {
            return true;
        }
        return false;
    }

    public boolean isUnArmedBomb(int _x, int _y) {
        if (tableau[_y][_x] < 0) {
            return true;
        }
        return false;
    }

    public int getMagnetisme(int _x, int _y) {
        return tableau[_y][_x];
    }
    
    public int getNombreCaseCachee()
    {
        int i =0;
        for(boolean [] tmp: tableauZoneRevellee)
            for (boolean tmp2: tmp)
            {
            if (tmp2==false)i++;
            }
    return i;
    }
    
    public int getNombreDeMine()
    {
        int i =0;
        for(int [] tmp: tableau)
            for (int tmp2: tmp)
            {
            if (tmp2>8)i++;
            }
    return i;
    }
    
    private void entourerLesMinesDeMagnetismes(int[][] tab, LinkedList<Point> positionMines) {
        for (Point tmp : positionMines) {
            System.out.print(tmp);
            
            int xSupGauche = tmp.x - 1;
            int ySupGauche = tmp.y - 1;
            
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (xSupGauche + i >= 0 && xSupGauche + i < tab[0].length
                            && ySupGauche + j >= 0 && ySupGauche + j < tab.length) {
                        if (tab[ySupGauche + j][xSupGauche + i] >= 0 && tab[ySupGauche + j][xSupGauche + i] < 8) {
                            tab[ySupGauche + j][xSupGauche + i]++;
                            
                        }
                    }
                    
                }
            }
            
            
        }
        
    }
}
